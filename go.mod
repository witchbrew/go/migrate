module gitlab.com/witchbrew/go/migrate

go 1.14

require (
	github.com/cenkalti/backoff/v4 v4.0.2
	github.com/lib/pq v1.7.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.19.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/witchbrew/go/sqltemplate v0.0.0-20201003202731-5ff7ea0de46c
	gitlab.com/witchbrew/go/sqlutils v0.0.0-20201003202214-4bc16d2bb29f
)
