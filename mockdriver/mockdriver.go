package mockdriver

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/witchbrew/go/migrate/migration"
	"gitlab.com/witchbrew/go/migrate/version"
)

type MockDriver struct {
	mock.Mock
}

func (d *MockDriver) Check() (bool, error) {
	args := d.Called()
	return args.Bool(0), args.Error(1)
}

func (d *MockDriver) Init() error {
	args := d.Called()
	return args.Error(0)
}

func (d *MockDriver) Version() (*version.Version, error) {
	args := d.Called()
	err := args.Error(1)
	if err != nil {
		return nil, err
	}
	return args.Get(0).(*version.Version), nil
}

func (d *MockDriver) SetVersion(v *version.Version) error {
	args := d.Called(v)
	return args.Error(0)
}

func (d *MockDriver) Run(f migration.Func) error {
	return nil
}

func New() *MockDriver {
	return &MockDriver{}
}
