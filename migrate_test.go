package migrate_test

import (
	"context"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/migrate"
	"gitlab.com/witchbrew/go/migrate/migration"
	"gitlab.com/witchbrew/go/migrate/mockdriver"
	"gitlab.com/witchbrew/go/migrate/testdriver"
	"gitlab.com/witchbrew/go/migrate/version"
	"testing"
	"time"
)

func TestSimpleEmptyMigration(t *testing.T) {
	m := migrate.New(testdriver.New(), migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return nil
		},
		Down: func(ctx context.Context) error {
			return nil
		},
	}})
	err := m.Init()
	require.Nil(t, err)
	v, err := m.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 0, Dirty: false}, v)
	err = m.Up()
	require.Nil(t, err)
	v, err = m.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 1, Dirty: false}, v)
	err = m.Down()
	require.Nil(t, err)
	v, err = m.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 0, Dirty: false}, v)
}

func TestZeroSteps(t *testing.T) {
	m := migrate.New(testdriver.New(), migrate.Migrations{})
	err := m.Init()
	require.Nil(t, err)
	v, err := m.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 0, Dirty: false}, v)
	err = m.Steps(0)
	require.Nil(t, err)
}

func TestVersionErr(t *testing.T) {
	drv := mockdriver.New()
	m := migrate.New(drv, migrate.Migrations{})
	drvErr := errors.New("mock error")
	expected := "error getting version: mock error"
	drv.On("Check").Return(true, nil)
	drv.On("Init").Return(nil)
	drv.On("Version").Return(nil, drvErr)
	err := m.Init()
	require.Nil(t, err)
	err = m.Up()
	assert.NotNil(t, err)
	assert.Equal(t, expected, err.Error())
	err = m.Down()
	assert.NotNil(t, err)
	assert.Equal(t, expected, err.Error())
	err = m.Steps(1)
	assert.NotNil(t, err)
	assert.Equal(t, expected, err.Error())
}

func TestSetDirtyVersionErr(t *testing.T) {
	drv := mockdriver.New()
	m := migrate.New(drv, migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return nil
		},
	}})
	drv.On("Check").Return(true, nil)
	drv.On("Init").Return(nil)
	drv.On("Version").Return(&version.Version{
		Value: 0,
		Dirty: false,
	}, nil)
	drvErr := errors.New("mock error")
	drv.On("SetVersion", &version.Version{Value: 1, Dirty: true}).Return(drvErr)
	err := m.Init()
	require.Nil(t, err)
	err = m.Up()
	assert.NotNil(t, err)
	assert.Equal(t, "error setting dirty version 1: mock error", err.Error())
}

func TestErrDuringUpMigration(t *testing.T) {
	m := migrate.New(testdriver.New(), migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return errors.New("up mock error")
		},
	}})
	err := m.Init()
	require.Nil(t, err)
	err = m.Up()
	assert.NotNil(t, err)
	assert.Equal(t, "failed to migrate from v0 to v1: up mock error", err.Error())
}

func TestErrDuringDownMigration(t *testing.T) {
	m := migrate.New(testdriver.New(), migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return nil
		},
		Down: func(ctx context.Context) error {
			return errors.New("down mock error")
		},
	}})
	err := m.Init()
	require.Nil(t, err)
	err = m.Up()
	assert.Nil(t, err)
	err = m.Down()
	assert.NotNil(t, err)
	assert.Equal(t, "failed to migrate from v1 to v0: down mock error", err.Error())
}

func TestSetCleanVersionErr(t *testing.T) {
	drv := mockdriver.New()
	m := migrate.New(drv, migrate.Migrations{&migration.Migration{
		Up: func(ctx context.Context) error {
			return nil
		},
		Down: func(ctx context.Context) error {
			return nil
		},
	}})
	drv.On("Check").Return(true, nil)
	drv.On("Init").Return(nil)
	drv.On("Version").Return(&version.Version{
		Value: 0,
		Dirty: false,
	}, nil)
	drv.On("SetVersion", &version.Version{Value: 1, Dirty: true}).Return(nil)
	drvErr := errors.New("mock error")
	drv.On("SetVersion", &version.Version{Value: 1, Dirty: false}).Return(drvErr)
	err := m.Init()
	require.Nil(t, err)
	err = m.Up()
	assert.NotNil(t, err)
	assert.Equal(t, "error setting clean version 1: mock error", err.Error())
}

func TestErrDirtyDatabase(t *testing.T) {
	drv := testdriver.New()
	m := migrate.New(drv, migrate.Migrations{})
	err := m.Init()
	require.Nil(t, err)
	err = drv.SetVersion(&version.Version{Value: 0, Dirty: true})
	require.Nil(t, err)
	err = m.Up()
	require.NotNil(t, err)
	require.Equal(t, "the database is dirty at version 0", err.Error())
}

func TestDatabaseIsNotReady(t *testing.T) {
	drv := mockdriver.New()
	m := migrate.New(drv, migrate.Migrations{&migration.Migration{}})
	drv.On("Check").Return(false, nil)
	err := m.WaitReady(time.Millisecond, 10)
	require.NotNil(t, err)
	require.Equal(t, "timeout waiting for driver: driver is still not ready", err.Error())
}

func TestCheckErr(t *testing.T) {
	drv := mockdriver.New()
	m := migrate.New(drv, migrate.Migrations{&migration.Migration{}})
	drv.On("Check").Return(false, errors.New("unexpected check err"))
	err := m.WaitReady(time.Millisecond, 10)
	require.NotNil(t, err)
	require.Equal(t, "timeout waiting for driver: unexpected check err", err.Error())
}
