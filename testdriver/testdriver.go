package testdriver

import (
	"context"
	"gitlab.com/witchbrew/go/migrate/driver"
	"gitlab.com/witchbrew/go/migrate/migration"
	"gitlab.com/witchbrew/go/migrate/version"
)

type testDriver struct {
	v *version.Version
}

func (d *testDriver) Check() (bool, error) {
	return true, nil
}

func (d *testDriver) Init() error {
	if d.v == nil {
		d.v = &version.Version{
			Value: 0,
			Dirty: false,
		}
	}
	return nil
}

func (d *testDriver) Version() (*version.Version, error) {
	return d.v, nil
}

func (d *testDriver) SetVersion(v *version.Version) error {
	d.v = v
	return nil
}

func (d *testDriver) Run(f migration.Func) error {
	return f(context.Background())
}

func New() driver.Driver {
	return &testDriver{}
}
