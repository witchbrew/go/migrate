package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/require"
	"gitlab.com/witchbrew/go/migrate"
	"gitlab.com/witchbrew/go/migrate/postgres"
	"gitlab.com/witchbrew/go/migrate/version"
	"gitlab.com/witchbrew/go/sqlutils/sqltesting"
	"os"
	"testing"
	"time"
)

func newDB(t *testing.T) *sql.DB {
	user := os.Getenv("TEST_PG_USERNAME")
	password := os.Getenv("TEST_PG_PASSWORD")
	host := os.Getenv("TEST_PG_HOST")
	database := os.Getenv("TEST_PG_DATABASE")
	connStr := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		user,
		password,
		host,
		database,
	)
	db, err := sql.Open("postgres", connStr)
	require.Nil(t, err)
	return db
}

func TestSimpleMigration(t *testing.T) {
	db := newDB(t)
	defer sqltesting.SafelyCloseDB(t, db)
	m := migrate.New(postgres.Driver(db, "test_version"), migrate.Migrations{
		{
			Up: func(ctx context.Context) error {
				db := postgres.ContextDB(ctx)
				_, err := db.Exec(`CREATE TABLE IF NOT EXISTS "some" (string VARCHAR PRIMARY KEY)`)
				require.Nil(t, err)
				return nil
			},
			Down: func(ctx context.Context) error {
				db := postgres.ContextDB(ctx)
				_, err := db.Exec(`DROP TABLE IF EXISTS "some"`)
				require.Nil(t, err)
				return nil
			},
		},
	})
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	m.Logger = log.Logger
	err := m.Init()
	require.Nil(t, err)
	err = m.Up()
	require.Nil(t, err)
	v, err := m.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 1, Dirty: false}, v)

	_, err = db.Exec(`INSERT INTO "some" VALUES ('hello world')`)
	require.Nil(t, err)
	var stringValue string
	err = db.QueryRow(`SELECT string FROM "some"`).Scan(&stringValue)
	require.Nil(t, err)
	require.Equal(t, "hello world", stringValue)

	err = m.Down()
	require.Nil(t, err)
	v, err = m.Version()
	require.Nil(t, err)
	require.Equal(t, &version.Version{Value: 0, Dirty: false}, v)
}

func TestCheckConnectionRefused(t *testing.T) {
	badPortConnStr := "postgres://user:password@127.0.0.1:111/postgres?sslmode=disable"
	db, err := sql.Open("postgres", badPortConnStr)
	require.Nil(t, err)
	defer sqltesting.SafelyCloseDB(t, db)
	m := migrate.New(postgres.Driver(db, "test_version"), migrate.Migrations{})
	err = m.WaitReady(time.Millisecond, 10)
	require.NotNil(t, err)
	require.Equal(t, "timeout waiting for driver: driver is still not ready", err.Error())
}

func TestCheckBadName(t *testing.T) {
	badNameDBConnStr := "postgres://user:password@obviously-bad-name/postgres?sslmode=disable"
	db, err := sql.Open("postgres", badNameDBConnStr)
	require.Nil(t, err)
	defer sqltesting.SafelyCloseDB(t, db)
	m := migrate.New(postgres.Driver(db, "test_version"), migrate.Migrations{})
	err = m.WaitReady(time.Millisecond, 10)
	require.NotNil(t, err)
	require.Contains(t, err.Error(), "timeout waiting for driver: driver is still not ready")
}