package migrate

import (
	"github.com/cenkalti/backoff/v4"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"gitlab.com/witchbrew/go/migrate/driver"
	"gitlab.com/witchbrew/go/migrate/migration"
	"gitlab.com/witchbrew/go/migrate/version"
	"time"
)

type Migrations = []*migration.Migration

type Migrate struct {
	drv        driver.Driver
	migrations Migrations
	Logger     zerolog.Logger
}

func New(drv driver.Driver, migrations Migrations) *Migrate {
	return &Migrate{
		drv:        drv,
		migrations: migrations,
		Logger:     zerolog.Nop(),
	}
}

func (m *Migrate) WaitReady(backOff time.Duration, retries uint64) error {
	bo := backoff.WithMaxRetries(backoff.NewConstantBackOff(backOff), retries)
	err := backoff.RetryNotify(func() error {
		ok, err := m.drv.Check()
		if err != nil {
			return backoff.Permanent(err)
		}
		if ok {
			return nil
		} else {
			return errors.New("driver is still not ready")
		}
	}, bo, func(err error, duration time.Duration) {
		m.Logger.Warn().
			Float64("waitSeconds", float64(duration)/float64(time.Second)).
			Msg("driver is not ready")
	})
	if err != nil {
		return errors.Wrap(err, "timeout waiting for driver")
	}
	return nil
}

func (m *Migrate) Init() error {
	return m.drv.Init()
}

func (m *Migrate) steps(initialVersion *version.Version, steps int) error {
	if initialVersion.Dirty {
		return errors.Errorf("the database is dirty at version %d", initialVersion.Value)
	}
	m.
		Logger.
		Info().
		Int("from", initialVersion.Value).
		Int("to", initialVersion.Value+steps).
		Msg("running migrations")
	var step int
	if steps > 0 {
		step = 1
	} else if steps < 0 {
		step = -1
	} else {
		return nil
	}
	startVerValue := initialVersion.Value + step
	stopVerValue := initialVersion.Value + steps + step
	for nextVerValue := startVerValue; nextVerValue != stopVerValue; nextVerValue += step {
		nextVer := &version.Version{
			Value: nextVerValue,
			Dirty: true,
		}
		vLogger := m.Logger.With().Int("version", nextVerValue).Logger()
		vLogger.Info().Msg("setting dirty flag")
		err := m.drv.SetVersion(nextVer)
		if err != nil {
			vLogger.Error().Err(err)
			return errors.Wrapf(err, "error setting dirty version %d", nextVerValue)
		}
		if step == 1 {
			vLogger.Info().Msg("executing migration up")
			err = m.drv.Run(m.migrations[nextVerValue-1].Up)
		} else {
			vLogger.Info().Msg("executing migration down")
			err = m.drv.Run(m.migrations[nextVerValue].Down)
		}
		if err != nil {
			vLogger.Error().Err(err)
			return errors.Wrapf(err, "failed to migrate from v%d to v%d", nextVerValue-step, nextVerValue)
		}
		nextVer.Dirty = false
		vLogger.Info().Msg("setting clean flag")
		err = m.drv.SetVersion(nextVer)
		if err != nil {
			vLogger.Error().Err(err)
			return errors.Wrapf(err, "error setting clean version %d", nextVerValue)
		}
	}
	return nil
}

func (m *Migrate) Version() (*version.Version, error) {
	return m.drv.Version()
}

func versionErr(err error) error {
	return errors.Wrap(err, "error getting version")
}

func (m *Migrate) Steps(steps int) error {
	v, err := m.drv.Version()
	if err != nil {
		return versionErr(err)
	}
	return m.steps(v, steps)
}

func (m *Migrate) Up() error {
	v, err := m.drv.Version()
	if err != nil {
		return versionErr(err)
	}
	return m.steps(v, len(m.migrations)-v.Value)
}

func (m *Migrate) Down() error {
	v, err := m.drv.Version()
	if err != nil {
		return versionErr(err)
	}
	return m.steps(v, -v.Value)
}
