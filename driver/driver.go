package driver

import (
	"gitlab.com/witchbrew/go/migrate/migration"
	"gitlab.com/witchbrew/go/migrate/version"
)

type Driver interface {
	// Check is a hacky way to determine whether a DB is available.
	// It is required mostly for CI purposes.
	Check() (bool, error)
	Init() error
	Version() (*version.Version, error)
	SetVersion(*version.Version) error
	Run(migration.Func) error
}
