package postgres

import (
	"context"
	"database/sql"
)

func ContextDB(ctx context.Context) *sql.DB {
	return ctx.Value("db").(*sql.DB)
}
