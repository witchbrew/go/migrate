package postgres

import (
	"database/sql"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestPostgresDriver_Check_Refused(t *testing.T) {
	connStr := "postgres://user:password@localhost:54321/postgres?sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	require.Nil(t, err)
	drv := Driver(db, "test-table")
	check, err := drv.Check()
	require.Nil(t, err)
	require.False(t, check)
}

func TestPostgresDriver_Check_BadHost(t *testing.T) {
	connStr := "postgres://user:password@bad-host/postgres?sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	require.Nil(t, err)
	drv := Driver(db, "test-table")
	check, err := drv.Check()
	require.Nil(t, err)
	require.False(t, check)
}
