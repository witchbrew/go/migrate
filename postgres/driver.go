package postgres

import (
	"context"
	"database/sql"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/witchbrew/go/migrate/driver"
	"gitlab.com/witchbrew/go/migrate/migration"
	"gitlab.com/witchbrew/go/migrate/version"
	"gitlab.com/witchbrew/go/sqltemplate"
	"gitlab.com/witchbrew/go/sqlutils"
	"net"
)

type postgresDriver struct {
	db               *sql.DB
	versionTableName string
}

func (d *postgresDriver) Check() (bool, error) {
	var checkValue int
	err := d.db.QueryRow("SELECT 1").Scan(&checkValue)
	if err != nil {
		opNetErr, ok := err.(*net.OpError)
		if !ok {
			return false, err
		}
		if opNetErr.Net != "tcp" || opNetErr.Op != "dial" {
			return false, err
		}
		return false, nil
	}
	if checkValue != 1 {
		return false, errors.Errorf("expected check to scan 1 got %d instead", checkValue)
	}
	return true, nil
}

var versionTableT = sqltemplate.New(`
CREATE TABLE IF NOT EXISTS {{vT}}
(
	id INTEGER PRIMARY KEY,
	value INTEGER NOT NULL,
	dirty BOOLEAN NOT NULL
)
`)

var countVersionT = sqltemplate.New(`
SELECT COUNT(*) FROM {{vT}}
`)

var insertVersionT = sqltemplate.New(`
INSERT INTO {{vT}} (id, value, dirty) VALUES (42, $1, $2)
`)

func (d *postgresDriver) ensureVersionRow() error {
	tx, err := d.db.Begin()
	if err != nil {
		return errors.Wrap(err, "failed to start transaction")
	}
	row := tx.QueryRow(countVersionT.Render(sqltemplate.Params{"vT": d.versionTableName}))
	var rowCount int
	err = row.Scan(&rowCount)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to scan number of rows")
	}
	if rowCount == 1 {
		err = tx.Commit()
		if err != nil {
			return errors.Wrap(err, "failed to commit")
		}
		return nil
	}
	if rowCount > 1 {
		return sqlutils.RollbackTxErr(tx, errors.Errorf("row count in version > 1: %d", rowCount))
	}
	_, err = tx.Exec(insertVersionT.Render(sqltemplate.Params{"vT": d.versionTableName}), 0, false)
	if err != nil {
		return sqlutils.RollbackTxErrWrap(tx, err, "failed to insert initial version")
	}
	err = tx.Commit()
	if err != nil {
		return errors.Wrap(err, "failed to commit")
	}
	return nil
}

func (d *postgresDriver) ensureVersionTable() error {
	_, err := d.db.Exec(versionTableT.Render(sqltemplate.Params{"vT": d.versionTableName}))
	if err != nil {
		return errors.Wrap(err, "failed to create versions table")
	}
	err = d.ensureVersionRow()
	if err != nil {
		return errors.Wrap(err, "failed to ensure initial migration")
	}
	return nil
}

func (d *postgresDriver) Init() error {
	return d.ensureVersionTable()
}

var selectVersionT = sqltemplate.New(`
SELECT value, dirty FROM {{vT}} WHERE id=42
`)

func (d *postgresDriver) Version() (*version.Version, error) {
	v := &version.Version{}
	row := d.db.QueryRow(selectVersionT.Render(sqltemplate.Params{"vT": d.versionTableName}))
	err := row.Scan(&v.Value, &v.Dirty)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get version")
	}
	return v, nil
}

var updateVersionT = sqltemplate.New(`
UPDATE {{vT}}
SET value=$1, dirty=$2
WHERE id=42
`)

func (d *postgresDriver) SetVersion(v *version.Version) error {
	_, err := d.db.Exec(updateVersionT.Render(sqltemplate.Params{
		"vT": d.versionTableName,
	}), v.Value, v.Dirty)
	if err != nil {
		return errors.Wrap(err, "failed to update version table")
	}
	return nil
}

func (d *postgresDriver) Run(f migration.Func) error {
	ctx := context.Background()
	return f(context.WithValue(ctx, "db", d.db))
}

func Driver(db *sql.DB, versionTableName string) driver.Driver {
	return &postgresDriver{
		db:               db,
		versionTableName: versionTableName,
	}
}
