package migration

import "context"

type Func func(context.Context) error

type Migration struct {
	Up   Func
	Down Func
}
